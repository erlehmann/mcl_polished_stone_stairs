--[[

mcl_polished_stone_stairs – Minetest mod to add polished stone stairs.
Copyright © 2022  Nils Dagsson Moskopp (erlehmann)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Dieses Programm hat das Ziel, die Medienkompetenz der Leser zu
steigern. Gelegentlich packe ich sogar einen handfesten Buffer
Overflow oder eine Format String Vulnerability zwischen die anderen
Codezeilen und schreibe das auch nicht dran.

]]--

local S = minetest.get_translator("mcl_polished_stone_stairs")

mcl_stairs.register_stair("stone", "mcl_core:stone_smooth",
	{
		pickaxey = 1,
		material_stone = 1,
	},
	{
		"mcl_stairs_stone_slab_top.png",
		"mcl_stairs_stone_slab_top.png",
		"mcl_stairs_stone_slab_top.png",
	},
	S("Polished Stone Stairs"),
	mcl_sounds.node_sound_stone_defaults(),
	0.8,
	0.8,
	nil,
	"mcl_core:stone_smooth"
)
